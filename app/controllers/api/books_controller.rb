class Api::BooksController < ApplicationController
  before_action :set_book, only: [:show, :edit, :update, :destroy]
  respond_to :json,:html

  def show
    render json: Book.find(params[:id])
  end

  def index
    render json: Book.all 
  end

  def create
    book = Book.new(book_params) 
    if book.save
      render json: book, status: 201, location: [:api, book] 
    else
      render json: { errors: book.errors }, status: 422
    end
  end

  def update
    book = Book.find(params[:id]) 

    if book.update(book_params)
      render json: book, status: 200, location: [:api, book] 
    else
      render json: { errors: book.errors }, status: 422
    end
  end

  def destroy
    book = Book.find(params[:id]) 
    book.destroy
    head 204
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    def book_params
      params.require(:book).permit(:title, :author, :publisher, :published_date, :description, :isbns, :page_count, :print_type, :category, :language, :image)
    end
end