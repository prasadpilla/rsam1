class BookSerializer < ActiveModel::Serializer
  attributes :id, :title, :author, :publisher, :published_date, :description, :isbns, :page_count, :print_type, :category, :language
end
